# frozen_string_literal: true

module GitlabSubscriptions
  module PromotionManagementHelper
    include ::MemberManagement::PromotionManagementUtils
  end
end
